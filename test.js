const tmi = require('tmi.js');
const client = new tmi.Client({
	options: { debug: true },
	connection: {
		reconnect: true,
		secure: true,
	},
	identity: {
		username: 'desarrolloutil_bot',
		password: '4t43ce1wt0th6derybk4q4tcy7le9h',
	},
	channels: ['desarrolloutil'],
});
client.connect().then(console.log('Connected')).catch(console.error);

client.on('message', (channel, tags, message, self) => {
	if (self) return;
	if (message.toLowerCase() === '!asd') {
		client.say(channel, `@${tags.username}, asd!`);
	}
});
