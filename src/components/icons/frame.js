import React from 'react';

export const Maximize = props => (
	<svg {...props} viewBox='0 0 15 15'>
		<rect strokeWidth='2px' fill='none' x='2.46' y='2.46' width='10.08' height='10.08' rx='0.33' />
	</svg>
);

export const Minimize = props => (
	<svg {...props} viewBox='0 0 15 15'>
		<line strokeWidth='2px' x1='1.61' y1='7.5' x2='13.39' y2='7.5' />
	</svg>
);

export const Close = props => (
	<svg {...props} viewBox='0 0 15 15'>
		<line strokeWidth='2px' x1='2.46' y1='2.46' x2='12.54' y2='12.54' />
		<line strokeWidth='2px' x1='2.46' y1='12.54' x2='12.54' y2='2.46' />
	</svg>
);

export const Restore = props => (
	<svg {...props} viewBox='0 0 15 15'>
		<rect strokeWidth='2px' x='6.26' y='2.46' width='6.5' height='6.5' rx='0.21' />
		<rect strokeWidth='2px' x='2.46' y='6.53' width='6.5' height='6.5' rx='0.21' />
	</svg>
);
