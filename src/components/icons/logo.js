import React from 'react';

const Logo = props => (
	<svg {...props} viewBox='0 0 30 30'>
		<rect
			id='Punto'
			fill='#2d364c'
			x='19.81'
			y='20.84'
			width='3.81'
			height='6.41'
			rx='1.9'
			transform='translate(-2.33 45.76) rotate(-90)'
		/>
		<rect
			id='D_Azul'
			data-name='D Azul'
			fill='#2d364c'
			x='4.75'
			y='17.59'
			width='17.34'
			height='6.17'
			rx='3.07'
			transform='translate(-7.01 6.88) rotate(-22.98)'
		/>
		<rect
			id='D_Verde'
			data-name='D Verde'
			fill='#64edbe'
			x='2.61'
			y='9.72'
			width='21.59'
			height='6.17'
			rx='3.08'
			transform='translate(13.7 -5.73) rotate(47.25)'
		/>
	</svg>
);

export default Logo;
