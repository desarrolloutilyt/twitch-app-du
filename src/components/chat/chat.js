import React, { useState, useEffect, useRef } from 'react';

import Message from './message';

const Chat = ({ client }) => {
	const [messages, setMessages] = useState([]);

	const chatRef = useRef();

	useEffect(() => {
		/**
		 * {String} channel #nombredelcanal
		 * {Object} tags Meta info del mensaje
		 * {String} message Texto del mensaje
		 *  -> username
		 *  ->
		 * {Boolean} self Si el mensaje es tuyo
		 */
		client.on('message', (channel, tags, message, self) => {
			console.log(tags);
			setMessages(oldMessages => [
				...oldMessages,
				{ username: tags.username, message, color: tags.color, emotes: tags.emotes },
			]);
		});

		return () => client.removeAllListeners();
	}, []);

	useEffect(() => {
		chatRef.current.scrollTo({ top: chatRef.current.scrollHeight });
	}, [messages]);

	return (
		<div ref={chatRef} className='w-full h-full overflow-x-hidden overscroll-y-auto'>
			{messages.map((msg, index) => (
				<Message key={index} {...msg} />
			))}
		</div>
	);
};

export default Chat;
