import React from 'react';

const Message = ({ username, message, color, emotes }) => {
	const formattedMessage = printWithEmotes(message, emotes);

	return (
		<div className='flexcol-c-s px-1 py-0_5 rounded-lg shadow-xl'>
			<span className='pb-0_5' style={{ color }}>
				{username}
			</span>
			<p className='break-all'>{formattedMessage}</p>
		</div>
	);
};

const printWithEmotes = (message, emotes) => {
	let entries = [];

	if (emotes) {
		entries = Object.entries(emotes).map(i => {
			const start = Number(i[1][0].split('-')[0]);
			const end = Number(i[1][0].split('-')[1]);

			return {
				url: getEmoteURL(i[0]),
				token: message.substr(start, end - start + 1),
			};
		});
	}

	return message.split(' ').map((i, index) => {
		const emote = entries.filter(j => j.token === i);

		return emote.length > 0 ? (
			<>
				<img key={Date.now()} className='inline' src={emote[0].url} />{' '}
			</>
		) : (
			`${i} `
		);
	});
};

const getEmoteURL = id => `http://static-cdn.jtvnw.net/emoticons/v1/${id}/1.0`;

export default Message;
