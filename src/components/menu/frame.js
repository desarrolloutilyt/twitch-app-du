import React, { useState, useEffect } from 'react';
import { Minimize, Maximize, Restore, Close } from '../icons/frame';
import { remote } from 'electron';

const currentWindow = remote.getCurrentWindow();

const Frame = () => {
	const [isMaximized, setisMaximized] = useState(false);

	useEffect(() => {
		currentWindow.addListener('maximize', () => setisMaximized(true));
		currentWindow.addListener('unmaximize', () => setisMaximized(false));
		currentWindow.addListener('close', () => currentWindow.removeAllListeners());

		return () => currentWindow.removeAllListeners();
	}, []);

	return (
		<header className='w-screen flex-sb-c h-2 bg-gray_terciary'>
			<div className='flex flex-1 ml-2 text-sm text-white select-none dragable'>Twitch APP</div>
			<div className='flex-sb-c h-2 mr-0_5 cursor-pointer'>
				<div className='flex-c-c w-2 h-full hover:bg-gray-600' onClick={minimize}>
					<Minimize className='w-1 stroke-white' />
				</div>
				<div className='flex-c-c w-2 h-full hover:bg-gray-600' onClick={() => maximize(isMaximized)}>
					{isMaximized ? <Restore className='w-1 stroke-white' /> : <Maximize className='w-1 stroke-white' />}
				</div>
				<div className='flex-c-c w-2 h-full hover:bg-red-600' onClick={close}>
					<Close className='w-1 stroke-white' />
				</div>
			</div>
		</header>
	);
};

const minimize = () => {
	currentWindow && currentWindow.minimize();
};

const maximize = isMaximized => {
	currentWindow && isMaximized ? currentWindow.restore() : currentWindow.maximize();
};

const close = () => {
	currentWindow && currentWindow.close();
	currentWindow.removeAllListeners();
};

export default Frame;
