import React, { useState, useEffect } from 'react';
import tmi from 'tmi.js';

import Frame from './menu/frame';
import Chat from './chat/chat';

import '../styles/index.css';

const App = () => {
	const [twitchClient, setTwitchClient] = useState();

	console.log(twitchClient);

	useEffect(() => {
		getTwitchClient(setTwitchClient);
	}, []);

	return (
		<>
			<Frame />
			<div style={{ height: 'calc(100vh - 2rem)' }} className='relative flex flex-auto'>
				<main className='absolute topleft bottomright overflow-x-hidden overflow-y-auto'>
					{twitchClient && <Chat client={twitchClient} />}
				</main>
			</div>
		</>
	);
};

const getTwitchClient = setTwitchClient => {
	const client = new tmi.Client({
		connection: {
			reconnect: true,
			secure: true,
		},
		identity: {
			username: 'desarrolloutil_bot',
			password: '4t43ce1wt0th6derybk4q4tcy7le9h',
		},
		channels: ['desarrolloutil'],
	});

	client.connect().then(setTwitchClient(client));
};

export default App;
