import React from "react";
import { render } from "react-dom";
import App from "./components/App";

// Create root div
let root = document.createElement("div");
root.id = "root";
document.body.appendChild(root);

// Render App into root div
render(<App />, document.getElementById("root"));
