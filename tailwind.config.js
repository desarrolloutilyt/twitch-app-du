const colors = {
	black: '#000000',
	white: '#FFFFFF',
	gray_primary: '#36393f',
	gray_secondary: '#2f3136',
	gray_secondary_alt: '#292b2f',
	gray_terciary: '#202225',
	overlay: '#00000091',
};

module.exports = {
	purge: {
		mode: 'all',
		content: ['.src/**/*.js'],
	},
	theme: {
		extend: {
			colors,
			fill: colors,
			stroke: colors,
		},
		screens: {
			xs: { max: '480px' },
			sm: { min: '481px', max: '768px' },
			md: { min: '769px', max: '1024px' },
			lg: { min: '1025px' },
			xssm: { max: '768px' },
			smmd: { min: '481px', max: '1024px' },
			mdlg: { min: '769px' },
		},
	},
	variants: {},
	plugins: require('tailwindcssdu'),
};
