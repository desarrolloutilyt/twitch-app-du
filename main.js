// Import parts of electron to use
const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');

//Reference to window object
let mainWindow;

// Check if running in development mode
const dev = process.env.NODE_ENV !== undefined && process.env.NODE_ENV === 'development';

//Create the browser window
const createWindow = () => {
	mainWindow = new BrowserWindow({
		width: 400,
		height: 800,
		minWidth: 400,
		minHeight: 800,
		frame: false,
		show: false,
		webPreferences: {
			webSecurity: false,
			nodeIntegration: true,
		},
	});

	// mainWindow.setMenu(null);

	let indexPath;

	if (dev && process.argv.indexOf('--noDevServer') === -1) {
		indexPath = url.format({
			protocol: 'http:',
			host: 'localhost:8080',
			pathname: 'index.html',
			slashes: true,
		});
	} else {
		indexPath = url.format({
			protocol: 'file:',
			pathname: path.join(__dirname, 'dist', 'index.html'),
			slashes: true,
		});
	}

	mainWindow.loadURL(indexPath);

	// Don't show until we are ready and loaded
	mainWindow.once('ready-to-show', () => {
		mainWindow.show();
	});

	// Emitted when the window is closed.
	mainWindow.on('closed', () => (mainWindow = null));
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow();
	}
});
